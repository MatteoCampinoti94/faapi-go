package faapi

import (
	"encoding/json"
	"net/http"
	"os"
)

// Return slice of cookies extracted from a json-formatted file
func loadCookies(file string) (cookies []*http.Cookie, err error) {
	cookiesFile, err := os.Open(file)
	if err != nil {
		return
	}
	defer cookiesFile.Close()

	err = json.NewDecoder(cookiesFile).Decode(&cookies)
	if err != nil {
		return
	}

	return
}

// Create an http.Request type with added cookies
func makeRequest(cookies []*http.Cookie) (req *http.Request, err error) {
	req, err = http.NewRequest("GET", hostGet, nil)
	if err != nil {
		return
	}

	for _, ck := range cookies {
		req.AddCookie(ck)
	}

	return
}
