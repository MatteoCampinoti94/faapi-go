package faapi

import (
	"net/http"
	"time"
)

// The FAAPI struct type contains a http.Cookie slice and a http.Request used by its methods
type FAAPI struct {
	Cookies  []*http.Cookie
	Request  *http.Request
	Interval time.Duration
	lastGet  time.Time
}

// LoadCookies will load cookies from a json formatted file
func (api *FAAPI) LoadCookies(cookiesFile string) (err error) {
	cookies, err := loadCookies(cookiesFile)
	if err != nil {
		return
	}

	api.Cookies = cookies

	return
}

// MakeRequest will use the cookies stored in the FAAPI.Cookies field to create a http.Request
func (api *FAAPI) MakeRequest() (err error) {
	req, err := makeRequest(api.Cookies)
	if err != nil {
		return
	}

	api.Request = req

	return
}

// GetSubmission downloads submission metadata from the server given a submission id
func (api *FAAPI) GetSubmission(id string) (sub Submission, err error) {
	sub, err = parseSubmission(api, id)
	return
}

// GetUser downloads user metadata from the server given a user id
func (api *FAAPI) GetUser(id string) (usr User, err error) {
	usr, err = parseUser(api, id)
	return
}

// GetGallery downloads submission metadata from a user's gallery page
func (api *FAAPI) GetGallery(user string, page int) (subs []Submission, next int, err error) {
	subs, next, err = parseUserFolders(api, pathGal, user, page)
	return
}

// GetScraps downloads submission metadata from a user's scraps page
func (api *FAAPI) GetScraps(user string, page int) (subs []Submission, next int, err error) {
	subs, next, err = parseUserFolders(api, pathScr, user, page)
	return
}

// GetFavorites downloads submission metadata from a user's favorites page
func (api *FAAPI) GetFavorites(user string, page int) (subs []Submission, next int, err error) {
	subs, next, err = parseUserFolders(api, pathFav, user, page)
	return
}

// GetSearch downloads submission metadata from a search page
func (api *FAAPI) GetSearch(args map[string]string, page int) (subs []Submission, next int, err error) {
	subs, next, err = parseSearch(api, args, page)

	return
}
