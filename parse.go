package faapi

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

/*
	Type Structs
*/

// The Submission struct contains the metadata of submissions found on the forum
type Submission struct {
	ID          int
	Title       string
	Author      string
	Date        string
	Tags        []string
	Description string
	FileURL     string
	File        []byte
}

// The User struct contains metadata found on a user's page
type User struct {
	Username string
	Userid   string
	Title    string
	Status   string
	Date     string
	Profile  string
}

/*
	Main Functions
*/

// Download submission metadata from the server given a submission id
func parseSubmission(api *FAAPI, id string) (sub Submission, err error) {
	res, err := get(api, pathSub+id, nil)
	if err != nil {
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		return
	} else if doc.Find("title").Contents().Text() == "System Error" {
		err = fmt.Errorf("The submission you are trying to find is not in our database")
		return
	}

	sub.ID, _ = strconv.Atoi(id)

	// Submission Title
	doc.Find("div h2").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "submission-title-header" {
			sub.Title = itm.Contents().Text()
		}
	})

	// Submission Author
	sub.Author = doc.Find("div section div div div a h2").First().Contents().Text()

	// Submission Date
	doc.Find("div div span").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "popup_date" {
			sub.Date, _ = itm.Attr("title")
			convertDate(&sub.Date)
		}
	})

	// Submission Tags
	doc.Find("div div span").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "tags" {
			sub.Tags = append(sub.Tags, itm.Contents().Text())
		}
	})
	sub.Tags = sub.Tags[0 : len(sub.Tags)/2]

	// Submission Description
	doc.Find("div div section div div div").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "submission-description-container link-override" {
			sub.Description, _ = itm.Html()
			sub.Description = strings.Join(strings.Split(sub.Description, "</div>")[1:], "</div>")
			sub.Description = strings.TrimSpace(sub.Description)
			sub.Description = strings.Replace(sub.Description, "src=\"//", "src=\"http://", -1)
		}
	})

	// File link
	doc.Find("div div div div a").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "button download-logged-in" {
			sub.FileURL, _ = itm.Attr("href")
			sub.FileURL = "http:" + sub.FileURL
		}
	})

	return
}

// Download user metadata from the server given a user id
func parseUser(api *FAAPI, id string) (user User, err error) {
	res, err := get(api, pathUsr+id, nil)
	if err != nil {
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		return
	} else if doc.Find("title").Contents().Text() == "System Error" {
		err = fmt.Errorf("This user cannot be found")
		return
	}

	user.Userid = strings.ToLower(id)

	// Username & Status
	doc.Find("body div div div div div div div").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "userpage-flex-item username" {
			re, _ := regexp.Compile("[\x21-\x7D]+")
			user.Username = re.FindString(itm.Find("h2 span").First().Contents().Text())

			status := re.ReplaceAllString(itm.Find("h2 span").First().Contents().Text(), "")
			switch status {
			case "∞":
				user.Status = "deceased"
			case "!":
				user.Status = "suspended"
			case "~":
				user.Status = "active"
			default:
				user.Status = fmt.Sprintf("unknown '%s'", status)
			}
		}
	})

	// User title & join date
	doc.Find("body div div div div div div div").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "userpage-flex-item username" {
			re, _ := regexp.Compile("([^\n]+)$")

			text := re.FindString(itm.Contents().Text())
			tmp := strings.Split(text, "|")

			titl := tmp[:len(tmp)-1]
			user.Title = strings.TrimSpace(strings.Join(titl, "|"))

			date := strings.Split(tmp[len(tmp)-1], ": ")[1]
			convertDate(&date)
			user.Date = strings.TrimSpace(date)
		}
	})

	// User profile
	doc.Find("body div div div section div").Each(func(i int, itm *goquery.Selection) {
		if class, exist := itm.Attr("class"); exist && class == "userpage-layout-profile-container link-override" {
			user.Profile, _ = itm.Html()
			user.Profile = strings.TrimSpace(user.Profile)
		}
	})

	return
}

// Download user gallery
func parseUserFolders(api *FAAPI, path string, user string, page int) (subs []Submission, next int, err error) {
	getPath := path + user + "/"

	if path == pathFav && page == 1 {
		getPath += "0/next"
	} else if path == pathFav {
		getPath += strconv.Itoa(page) + "/next"
	} else {
		getPath += strconv.Itoa(page)
	}

	res, err := get(api, getPath, nil)
	if err != nil {
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		return
	} else if class, exist := doc.Find("body div div div section").Attr("class"); exist && class == "aligncenter notice-message" {
		err = fmt.Errorf("The username '%s' could not be found", user)
		return
	}

	subs = parsePage(doc)

	if n := parseNext(doc, path); n != 0 {
		next = n
	} else {
		next = -1
	}

	return
}

// Download search
func parseSearch(api *FAAPI, args map[string]string, page int) (subs []Submission, next int, err error) {
	argsTmp := args
	argsTmp["page"] = strconv.Itoa(page)

	res, err := get(api, pathSrc, argsTmp)
	if err != nil {
		return
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		return
	}

	subs = parsePage(doc)

	if parseNext(doc, pathSrc) == 1 {
		next = page + 1
	} else {
		next = -1
	}

	return
}

/*
	Auxiliary functions
*/

// Convert human-readable date (e.g. Nov 2nd, 2018) into compact date (e.g. 2018-11-2)
func convertDate(date *string) {
	re, _ := regexp.Compile("([A-Za-z]{3}|[0-9]+)")
	dateTmp := re.FindAllString(*date, -1)

	*date = dateTmp[2]

	switch dateTmp[0] {
	case "Jan":
		*date += "-01-"
	case "Feb":
		*date += "-02-"
	case "Mar":
		*date += "-03-"
	case "Apr":
		*date += "-04-"
	case "May":
		*date += "-05-"
	case "Jun":
		*date += "-06-"
	case "Jul":
		*date += "-07-"
	case "Aug":
		*date += "-08-"
	case "Sep":
		*date += "-09-"
	case "Oct":
		*date += "-10-"
	case "Nov":
		*date += "-11-"
	case "Dec":
		*date += "-12-"
	}

	*date += fmt.Sprintf("%02s", dateTmp[1])
}

// Parse a page for figures tags and returns the list of submission metadata
func parsePage(doc *goquery.Document) (subs []Submission) {
	doc.Find("figure").Each(func(i int, itm *goquery.Selection) {
		var sub Submission

		tmp, _ := itm.Attr("id")
		tmp = strings.Split(tmp, "-")[1]

		sub.ID, _ = strconv.Atoi(tmp)

		itm = itm.Find("figcaption p a")

		sub.Title = itm.First().AttrOr("title", "")
		sub.Author = itm.Last().AttrOr("title", "")

		subs = append(subs, sub)
	})

	return
}

// Find the link to the next page
func parseNext(doc *goquery.Document, path string) (next int) {
	if path == pathGal || path == pathScr {
		doc.Find("form").Each(func(i int, itm *goquery.Selection) {
			if m, a := itm.AttrOr("method", ""), itm.AttrOr("action", ""); m == "get" && a != "" {
				re, _ := regexp.Compile("[0-9]+")
				n := re.FindString(a)
				b := itm.Find("button").First()
				if n != "" && b.AttrOr("type", "") == "submit" && b.Text() == "Next" {
					next, _ = strconv.Atoi(n)
					return
				}
			}
		})
	} else if path == pathFav {
		doc.Find("div div a").Each(func(i int, itm *goquery.Selection) {
			if itm.HasClass("button mobile-button right") {
				re, _ := regexp.Compile("[0-9]+")
				tmp := re.FindString(itm.AttrOr("href", ""))
				if tmp != "" {
					next, _ = strconv.Atoi(tmp)
					return
				}
			}
		})
	} else if path == pathSrc {
		doc.Find("div div input").Each(func(i int, itm *goquery.Selection) {
			if itm.AttrOr("name", "") == "next_page" && itm.HasClass("button") && !itm.HasClass("button hidden") {
				next = 1
			}
		})
	}

	return
}
