package faapi

import (
	"bytes"
	"fmt"
	"net/http"
	"time"

	"github.com/cardigann/go-cloudflare-scraper"
)

var hostGet = "https://www.furaffinity.net" // Host address for GET requests
var pathSub = "/view/"                      // Path to submission page
var pathJrn = "/journal/"                   // Path to journal page
var pathUsr = "/user/"                      // Path to user page
var pathGal = "/gallery/"                   // Path to gallery section
var pathScr = "/scraps/"                    // Path to scraps section
var pathFav = "/favorites/"                 // Path to favorites section
var pathJrs = "/journals/"                  // Path to journals section
var pathSrc = "/search/"                    // Path to search

// Get response from a GET request to a given path
func get(api *FAAPI, path string, params map[string]string) (res *http.Response, err error) {
	defer func(lastGet *time.Time) { *lastGet = time.Now() }(&api.lastGet)

	req := api.Request

	if params != nil {
		urlParams := req.URL.Query()
		for k, v := range params {
			urlParams.Add(k, v)
		}
		req.URL.RawQuery = urlParams.Encode()
	}

	req.URL.Path = path

	scraper, err := scraper.NewTransport(http.DefaultTransport)
	if err != nil {
		return
	}

	client := &http.Client{Timeout: 30 * time.Second, Transport: scraper}

	if api.Interval > 0 {
		time.Sleep(api.Interval - time.Since(api.lastGet))
	}

	res, err = client.Do(req)
	if err != nil {
		return
	} else if res.StatusCode != 200 {
		res.Body.Close()
		err = fmt.Errorf("Error: %d", res.StatusCode)
		return
	}

	return
}

/*
	Submission Methods
*/

// GetFile method downloads submission file inside the struct
func (sub *Submission) GetFile() (err error) {
	if sub.FileURL == "" {
		return fmt.Errorf("Submission FileURL missing")
	}

	resp, err := http.Get(sub.FileURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	sub.File = buf.Bytes()

	return
}
